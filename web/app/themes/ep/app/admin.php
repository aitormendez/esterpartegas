<?php

namespace App;

/**
 * https://github.com/soberwp/intervention
 */

use function \Sober\Intervention\intervention;

if (function_exists('\Sober\Intervention\intervention')) {
    // now you can use the function to call the required modules and their params
    intervention('remove-emoji');
    intervention('remove-howdy');
}

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});
