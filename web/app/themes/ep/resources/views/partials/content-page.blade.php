@php the_content() @endphp

@if (is_page('about'))
  <p>
    <a href="{{ $pdf->url }}" target="_blank">Download CV</a>
  </p>
@endif
