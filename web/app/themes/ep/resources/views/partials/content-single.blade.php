@php
$date = DateTime::createFromFormat('Ymd', $production_date);
$anio = $date->format('Y');
$date_end = DateTime::createFromFormat('Ymd', $end_production_date);
if (!$end_production_date == '') {
  $anio_end = $date_end->format('Y');
}
@endphp

<article @php post_class() @endphp>
  <header>
    <h1 class="entry-title">{!! get_the_title() . ', ' . $anio !!}
    @if (!$end_production_date == '')
      {{ ' - ' . $anio_end}}
    @endif</h1>
    @if (!$tecnical_sheet == '')
      <div class="ficha">
        {!! $tecnical_sheet !!}
      </div>
    @endif
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
