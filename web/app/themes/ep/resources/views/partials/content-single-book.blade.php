<article @php post_class() @endphp>
  <header>
    <h1 class="entry-title">{{ get_the_title() }}
  </header>
  <div class="entry-content">
    @if (has_post_thumbnail())
      <div class="portada">
        {{ the_post_thumbnail('medium') }}
      </div>
    @endif
    @php
      the_content();
      $file = get_field('pdf');
    @endphp
    @if ($file)
      <a href="{{ $file['url'] }}" target="_blank">PDF</a>
    @endif
  </div>
</article>
