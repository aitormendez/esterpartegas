<article @php post_class() @endphp>
  @if (has_post_thumbnail())
    <div class="portada">
      {{ the_post_thumbnail('medium') }}
    </div>
  @endif
  <div class="col-der">
    <header>
      <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    </header>
    <div class="entry-summary">
      @php
        the_content();
        $file = get_field('pdf');
      @endphp
      @if ($file)
        <a href="{{ $file['url'] }}" target="_blank">PDF</a>
      @endif
    </div>
  </div>
</article>
