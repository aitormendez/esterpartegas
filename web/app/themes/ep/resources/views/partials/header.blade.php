@if (is_admin_bar_showing())
  @php $adminbar_class = 'con-adminbar' @endphp
@else
  @php $adminbar_class = 'sin-adminbar' @endphp
@endif

<header class="banner">
  <div class="arriba {{ $adminbar_class }}">
    <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    <a id="hamb" class="closed">
      <i></i>
      <i></i>
      <i></i>
    </a>
  </div>
  <nav class="nav-primary">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new ep_submenu, 'menu_class' => 'nav']) !!}
    @endif
    <div class="mask closed {{ $adminbar_class }}"></div>
  </nav>
</header>
