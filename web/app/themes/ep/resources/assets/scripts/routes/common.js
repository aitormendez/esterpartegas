export default {
  init() {
    // JavaScript to be fired on all pages


    $(document).ready(function() {


      // HAMBURGUESA + NAV + BANNER
      // ------------

      var viewportWidth = $(window).width();
      if (viewportWidth <= 1000) {

        // esconder y mostrar banner cuando scroll

        var lastScrollTop = 0;

        $(window).scroll(function() {
          var scrollWin = $(window).scrollTop();

          // cambiar color
          if (scrollWin > 5) {
            $('.arriba').addClass('opaco');
          } else {
            $('.arriba').removeClass('opaco');
          }

          // detectar scroll direction
          if (scrollWin > lastScrollTop) {
            $('.arriba').addClass('hide');
          } else {
            $('.arriba').removeClass('hide');
          }
          lastScrollTop = scrollWin;

        });

        //menu hamburguesa y solapa

        $('.nav-primary, .mask').addClass('closed');
        $('#hamb').click(function() {
          $('body').toggleClass('noscroll');
          $(this).toggleClass('closed open');
          $('.nav-primary, .mask').toggleClass('closed open');
        });
      } else {
        $('.nav-primary, .mask').addClass('open');
        $('.nav-primary').scroll(function() {
          var scroll = $('.nav-primary').scrollTop();
          if (scroll > 5) {
            $('.mask').addClass('opaco');
          } else {
            $('.mask').removeClass('opaco');
          }
        });
      }

      /* eslint-disable */
      var catItems = ep.catItems;
      /* eslint-enable */

      catItems.forEach(function(e) {
        if (sessionStorage.getItem(e) == 'show') {
          $('#' + e).parent().find('ul').addClass('show');
          $('.show').css("display", "block");
        }
        // console.log('Guardado ' + e + ' = ' + sessionStorage.getItem(e));
      });


      $('.toggle').click(function(e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
        } else {
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
        }

        var elementId = $('.show').parent().find('a').attr('id');
        // console.log('elementId = ' + elementId);

        catItems.forEach(function(e) {
          if (e === elementId) {
            sessionStorage.setItem(e, 'show');
          } else {
            sessionStorage.setItem(e, 'hide');
          }
          // console.log(e + ' = ' + sessionStorage.getItem(e));
        });

      });


    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};