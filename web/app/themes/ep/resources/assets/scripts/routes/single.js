import 'lightgallery/dist/js/lightgallery.min.js';

export default {
  init() {

    $('.gallery').lightGallery({
      selector: 'a',
      mode: 'lg-fade',
      speed: 200,
      thumbContHeight: 60,
      hideBarsDelay: 500,
    });

  },
};