<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;

$options = [
	'supports' => [ 'title', 'editor', 'page-attributes', 'thumbnail' ],
	'capability_type' => 'post',
    'has_archive'           => true,
    'show_in_nav_menus'     => true,
];

$book = new PostType('book', $options);

$book->register();


$options_weight_tax = [
    'hierarchical' => false,
];

$weight = new Taxonomy('weight', $options_weight_tax);

$weight->register();

$book->taxonomy('weight');
