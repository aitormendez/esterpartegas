<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;

$options = [
	'supports' => [ 'title', 'editor', 'page-attributes', 'thumbnail' ],
	'capability_type' => 'post',
];
$names = [
    'name' => 'item_cv',
    'singular' => 'Item CV',
    'plural' => 'Items CV',
    'slug' => 'items_cv'
];

$item_cv = new PostType($names, $options);

$item_cv->register();


$options_epi_tax = [
    'hierarchical' => false,
];

$epigraph = new Taxonomy('epigraph', $options_epi_tax);

$epigraph->register();

$item_cv->taxonomy('epigraph');
